<style type="text/css">
.small-box {
    width: 21%;
    float: left;
    margin-left: 12px;
}
</style>
<div class="box-area">

	<div class="box">
		<h3 style="margin-left: 12px;">Add discount settings</h3>
		<form>
		<div class="small-box">
			<?php 
		$post_args = array(	'post_type' 		=> array( 'product', 'product_variation' ),
							'posts_per_page' 		=> -1,
							'lang' => array( 'nl', 'en' ),
							'orderby'         	=> 'title',
    						'order'           	=> 'DESC',
    						'exclude' 			=> $post->ID,
    						'post_status'		=> array( 'publish', 'private', 'draft' ),
    						'tax_query'			=> array(
								array(
									'taxonomy'	=> 'product_type',
									'field'		=> 'slug',
									'terms'		=> array( 'variable', 'grouped' ),
									'operator'	=> 'NOT IN'
								)
							),
							'suppress_filters' 	=> 0
							);
		$html ="";		
		$posts_array = get_posts( $post_args );
		if(count($posts_array)>0){
		$html .= '<select id="course-woocommerce-product-options" name="course_woocommerce_product" class="chosen_select widefat">' . "\n";
			$html .= '<option value="-">' . __( 'None', 'woothemes-sensei' ) . '</option>';
				$prev_parent_id = 0;
				foreach ( $posts_array as $post_item ) {

					if ( 'product_variation' == $post_item->post_type ) {

						$product_object = Sensei_WC_Utils::get_product( $post_item->ID );

						if ( empty( $product_object ) ) {
							// Product variation has been orphaned. Treat it like it has also been deleted.
							continue;
						}

						$parent_id = wp_get_post_parent_id( $post_item->ID );

                        if( sensei_check_woocommerce_version( '2.1' ) ) {
							$formatted_variation = wc_get_formatted_variation( Sensei_WC_Utils::get_variation_data( $product_object ), true );

						} else {
                            // fall back to pre wc 2.1
							$formatted_variation = woocommerce_get_formatted_variation( Sensei_WC_Utils::get_variation_data( $product_object ), true );

						}

                        $product_name = ucwords( $formatted_variation );
                        if ( empty( $product_name ) ) {
                            $product_name = __( 'Variation #', 'woothemes-sensei' ) . Sensei_WC_Utils::get_product_variation_id( $product_object );
                        }
					} else {

						$parent_id = false;
						$prev_parent_id = 0;
						$product_name = $post_item->post_title;

					}

					// Show variations in groups
					if( $parent_id && $parent_id != $prev_parent_id ) {

						if( 0 != $prev_parent_id ) {

							$html .= '</optgroup>';

						}
						$html .= '<optgroup label="' . get_the_title( $parent_id ) . '">';
						$prev_parent_id = $parent_id;

					} elseif( ! $parent_id && 0 == $prev_parent_id ) {

						$html .= '</optgroup>';

					}

					$html .= '<option value="' . esc_attr( absint( $post_item->ID ) ) . '"' . selected( $post_item->ID, $select_course_woocommerce_product, false ) . '>' . esc_html( $product_name ) . '</option>' . "\n";

				} // End For Loop

			$html .= '</select>' . "\n";
			if ( current_user_can( 'publish_product' )) {

				$html .= '<p>' . "\n";
					$html .= '<a href="' . admin_url( 'post-new.php?post_type=product' ) . '" title="' . esc_attr( __( 'Add a Product', 'woothemes-sensei' ) ) . '">' . __( 'Add a Product', 'woothemes-sensei' ) . '</a>' . "\n";
				$html .= '</p>'."\n";

			} // End If Statement


			echo $html;
		}else{

		}

		?>

		</div>
		<div class="small-box">
			<input type="text" placeholder="Buy Quantity">
		</div>
		<div class="small-box">
			<input type="text" placeholder="Discount Quantity">
		</div>
		<div class="small-box">
			<button type="button" class="button button-primary">Add to Discount</button>
		</div>
		
		</form>
	</div>
</div>