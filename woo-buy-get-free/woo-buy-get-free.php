<?php
/*
Plugin Name: Woo Buy Get Free
Author: Rakesh Kumar
Description: It enable the discount on the products like buy 2 get 1 free.
Author URI: postnidea.com
*/

define('WOO_PLUGIN_VERSION','0.0.1');
define('WOO_PLUGIN_PATH',plugin_dir_path(__FILE__));
define('WOO_PLUGIN_URI',plugin_dir_url(__FILE__));
define('WOO_PLUGIN_TEXTDOMAIN','WOO-GET-FREE');
include_once(WOO_PLUGIN_PATH."class.woo-buy-get-free.php");
new wooBuyGetFree();



