<?php

/*
Plugin Name: Book Parcel
Description: Test description
Author: Rakesh Kumar
Author URI: http://maatwerkonline.nl
*/
define('TEST_PLUGIN_VERSION','0.0.1');
define('TEST_PLUGIN_PATH',plugin_dir_path(__FILE__));
define('TEST_PLUGIN_URI',plugin_dir_url(__FILE__));
define('TEST_PLUGIN_TEXTDOMAIN','');
include_once TEST_PLUGIN_PATH.'class.test-plugin.php';
new testClass();
?>

<?php 

class testClass{

	private $options;

	public function __construct(){
		add_action('admin_menu',array($this,'add_settings_page'));
		add_action('admin_init',array($this,'init_settings'));

	}

	public function add_settings_page(){
		add_options_page("test","test","manage_options","test-slug",array($this,'magicBox'));
	}

	public function init_settings(){
		register_setting('test_group','test_values',array($this,'sanitize'));
		add_settings_section('test-section-id'," Please add parcel",array($this,'info'),'test-slug');
		add_settings_field('test11','test11',array($this,'test11_callback'),'test-slug','test-section-id');
	}

	public function test11_callback(){
		 printf(
            '<input type="text" id="test11" name="test_values[test11]" value="%s" />',
            isset( $this->options['test11'] ) ? esc_attr( $this->options['test11']) : ''
        );
	}

	public function info(){
		echo "please fill all values";
	}
	public function magicBox(){
		$this->options = get_option('test_values');
	?>
		<form action="options.php" method="post">
			<?php 			
			
			settings_fields('test_group'); 
			do_settings_sections( 'test-slug' );
            submit_button();
            
            ?>
		</form>

	<?php
	}


	public function sanitize($new_input){
		if(isset($new_input['shop_code'])){
			$new_input['shop_code'] = sanitize_text_field($new_input['shop_code']);
		}
		return $new_input;
	}
}


?>